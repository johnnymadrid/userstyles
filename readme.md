# Userstyles for certain sites

Install stylus extension for browser of your choise.

Stylus needs access to this site before the guided installation flow works.

## BBS.IO-TECH.FI

Userstyle for bbs.io-tech.fi

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-238b8b.svg)](https://gitlab.com/johnnymadrid/userstyles/-/raw/main/bbs.io-tech.fi.classic.gray.user.css)

[Open Sans -fonts](https://fonts.google.com/specimen/Open+Sans?query=Open+Sans)

## NHL66.IR

Userstyle for nhl66.ir

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-238b8b.svg)](https://gitlab.com/johnnymadrid/userstyles/-/raw/main/nhl66ir.user.css)
